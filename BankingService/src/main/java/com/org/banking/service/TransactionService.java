package com.org.banking.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.banking.models.BankAccount;
import com.org.banking.models.BankTransaction;
import com.org.banking.repository.BankAccountRepository;
import com.org.banking.repository.BankTransactionRepository;

@Service
public class TransactionService {
	
	@Autowired 
	BankTransactionRepository btrepo;
	
	@Autowired 
	BankAccountRepository brepo;

	public List<BankTransaction> findAll() {
		// TODO Auto-generated method stub
		return btrepo.findAll();
	}

	public BankTransaction save(BankTransaction trans) {
		return btrepo.save(trans);
	}

	public List<BankTransaction> getLastTransactions(Long cardnumber, int numberoftransactions) {
		// TODO Auto-generated method stub
		return btrepo.findLastTransactions(cardnumber, numberoftransactions);
	}

	public BankTransaction makePayment(long cardnumber, double amntcharged) {
		BankTransaction ba = new BankTransaction();
		ba.setFromcard(cardnumber);
		ba.setTocard(2323232323232323L);
		ba.setAmounttransfered(amntcharged);
		ba.setDateoftransfer(new Date());
		
		Optional<BankAccount> from = brepo.findByCardNumber(cardnumber);
		BankAccount bafrom = from.get();
		double bal = bafrom.getBalanceamount();		
		bal = bal - amntcharged;		
		bafrom.setBalanceamount(bal);		
		brepo.save(bafrom);
		
		Optional<BankAccount> toacct = brepo.findByCardNumber(2323232323232323L);
		BankAccount bato = toacct.get();
		double balto = bato.getBalanceamount();		
		balto = balto +amntcharged;		
		bato.setBalanceamount(balto);		
		brepo.save(bato);
		
		return btrepo.save(ba);		
	}

}
