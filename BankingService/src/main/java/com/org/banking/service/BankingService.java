package com.org.banking.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.banking.models.BankAccount;
import com.org.banking.models.Customer;
import com.org.banking.repository.BankAccountRepository;

@Service
public class BankingService {

	@Autowired
	BankAccountRepository brep;

	public BankAccount createAccount(Customer cust) {
		System.out.println("service123");
		LocalDate currDate = LocalDate.now();
		int month = currDate.getMonthValue();
		int day = currDate.getDayOfYear();
		int year = currDate.getYear();	
		
		double rand = Math.random();
		int randInt = (int)rand * 10000;
		
		String acctStr = day+""+month+""+year+""+randInt;		
		Long accountnumber = Long.parseLong(acctStr);		
		
		double rand1 = Math.random()+2345;
		int randInt1 = (int)rand;
		String cardStr = day+""+month+""+year+""+randInt;
		
		Long cardnumber = Long.parseLong(cardStr);		
		BankAccount bacct = new BankAccount();
		bacct.setAccountnumber(accountnumber);
		
		bacct.setBalanceamount(1000.00);
		bacct.setCardnumber(cardnumber);		
		bacct.setExpirydate(LocalDate.now());
		
		bacct.setName(cust.getFirstname()+","+cust.getLastname());
		bacct.setPhonenumber(cust.getPhonenumber());		
		bacct.setCvvnumber(100l);
		BankAccount savedAcct = brep.save(bacct);		
		return savedAcct;
	}

	public boolean findByCardNumber(Long cardnumber) {
		Optional<BankAccount> ba = brep.findByCardNumber(cardnumber);		
		return ba.isPresent()? true : false;
			
	}

}
