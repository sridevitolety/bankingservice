package com.org.banking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.banking.models.BankTransaction;

@Repository
public interface BankTransactionRepository extends JpaRepository<BankTransaction , Long>{
	
	@Query(value = "select * from bank_transaction where fromcard = ?1 limit ?2", nativeQuery = true)
	List<BankTransaction> findLastTransactions(Long cardnumber , int number);

}
