package com.org.banking.models;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BankAccount {
	
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private Double balanceamount;
	private Long phonenumber;
	
	@Column(unique = true)
	private Long accountnumber;	
	
	public Long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(Long phonenumber) {
		this.phonenumber = phonenumber;
	}

	@Column(unique = true)
	private Long cardnumber;
	private Long cvvnumber;
	private LocalDate expirydate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getBalanceamount() {
		return balanceamount;
	}
	public void setBalanceamount(Double balanceamount) {
		this.balanceamount = balanceamount;
	}
	public Long getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(Long accountnumber) {
		this.accountnumber = accountnumber;
	}
	public Long getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(Long cardnumber) {
		this.cardnumber = cardnumber;
	}
	public Long getCvvnumber() {
		return cvvnumber;
	}
	public void setCvvnumber(Long cvvnumber) {
		this.cvvnumber = cvvnumber;
	}
	public LocalDate getExpirydate() {
		return expirydate;
	}
	public void setExpirydate(LocalDate expirydate) {
		this.expirydate = expirydate;
	}
	

	@Override
	public String toString() {
		return "BankAccount [id=" + id + ", name=" + name + ", balanceamount=" + balanceamount + ", phonenumber="
				+ phonenumber + ", accountnumber=" + accountnumber + ", cardnumber=" + cardnumber + ", cvvnumber="
				+ cvvnumber + ", expirydate=" + expirydate + "]";
	}
	public BankAccount() {}
	public BankAccount(Long id, String name, Double balanceamount, Long phonenumber, Long accountnumber,
			Long cardnumber, Long cvvnumber, LocalDate expirydate) {
		super();
		this.id = id;
		this.name = name;
		this.balanceamount = balanceamount;
		this.phonenumber = phonenumber;
		this.accountnumber = accountnumber;
		this.cardnumber = cardnumber;
		this.cvvnumber = cvvnumber;
		this.expirydate = expirydate;
	}


}
