package com.org.banking.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BankTransaction {
	
	@Id
	@GeneratedValue
	private Long id;
	private Long fromcard;	
	public Long getFromcard() {
		return fromcard;
	}
	public void setFromcard(Long fromcard) {
		this.fromcard = fromcard;
	}
	public Long getTocard() {
		return tocard;
	}
	public void setTocard(Long tocard) {
		this.tocard = tocard;
	}
	private Long tocard;
	private Double amounttransfered;
	private Date dateoftransfer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getAmounttransfered() {
		return amounttransfered;
	}
	public void setAmounttransfered(Double amounttransfered) {
		this.amounttransfered = amounttransfered;
	}
	public Date getDateoftransfer() {
		return dateoftransfer;
	}
	public void setDateoftransfer(Date dateoftransfer) {
		this.dateoftransfer = dateoftransfer;
	}
	
	public BankTransaction() {}
	public BankTransaction(Long id, Long fromcard, Long tocard, Double amounttransfered, Date dateoftransfer) {
		super();
		this.id = id;
		this.fromcard = fromcard;
		this.tocard = tocard;
		this.amounttransfered = amounttransfered;
		this.dateoftransfer = dateoftransfer;
	}

}

