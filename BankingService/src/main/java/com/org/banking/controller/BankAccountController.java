package com.org.banking.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.banking.models.BankAccount;
import com.org.banking.models.Customer;
import com.org.banking.repository.BankAccountRepository;
import com.org.banking.service.BankingService;

@RestController
@RequestMapping("/banking")
public class BankAccountController {
	
	@Autowired 
	BankAccountRepository brepo;
	
	@Autowired 
	BankingService bserv;
	
	@GetMapping("")
	public List<BankAccount> getAccount(){
		return brepo.findAll();
	}
	
	@PostMapping("/registercustomer")
	public BankAccount registerCustomer(@RequestBody Customer cust) {		
		return bserv.createAccount(cust);
	}

	@GetMapping("/validateCardNumber/{cardnumber}")
	public boolean verifyCardNumber(@PathVariable Long cardnumber) {
		return bserv.findByCardNumber(cardnumber);			
	}

}
