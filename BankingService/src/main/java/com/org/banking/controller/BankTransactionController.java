package com.org.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.banking.models.BankTransaction;
import com.org.banking.service.TransactionService;

@RestController
@RequestMapping("/transaction")
public class BankTransactionController {
	
	@Autowired 
	TransactionService tserv;
	
	@GetMapping("")
	public List<BankTransaction> getTransactions(){
		return tserv.findAll();
	}
	
	@PostMapping("/makeTransaction")
	public BankTransaction makeTransaction(@RequestBody BankTransaction trans){		
		BankTransaction t = tserv.save(trans);
		return t;		
	}
	
	@GetMapping("/makePayment/{cardnumber}/{amntcharged}")
	public BankTransaction makePayment(@PathVariable long cardnumber , @PathVariable double amntcharged){		
		BankTransaction t = tserv.makePayment(cardnumber , amntcharged);
		return t;		
	}
	
	@GetMapping("/lastTransactions/{cardnumber}/{numberoftransactions}")
	public List<BankTransaction> getLastTransactions(@PathVariable Long cardnumber, @PathVariable int numberoftransactions){
		List<BankTransaction> t = tserv.getLastTransactions(cardnumber , numberoftransactions);
		return t;
	}

}
