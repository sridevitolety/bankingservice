package com.org.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.banking.models.Customer;
import com.org.banking.repository.CustomerRepository;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired 
	CustomerRepository crepo;
	
	@GetMapping("")
	public List<Customer> getCustomers(){
		return crepo.findAll();
	}
}
